<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('login');
});

Route::get('home', function () {
    return view('home');
});
Route::get('home',[App\Http\Controllers\PerpusController::class, 'home']);
Route::get('buku',[App\Http\Controllers\PerpusController::class, 'lihatbuku']);

Route::get('buku/tambah', function () {
    return view('buku.tambahbuku');
});

Route::get('anggota/tambah', function () {
    return view('anggota.tambahanggota');
});
Route::get('register', function () {
    return view('register');
});
Route::get('forgetpass', function () {
    return view('forgetpass');
});
Route::get('homepengguna', function () {
    return view('pengguna.homepengguna');
});
Route::get('pengguna/pinjambuku', function () {
    return view('pengguna.pinjambuku');
});



Route::get('buku/edit',[App\Http\Controllers\PerpusController::class, 'editbuku']);
Route::get('buku/hapus',[App\Http\Controllers\PerpusController::class, 'hapusbuku']);
Route::get('buku/edit/{kode_eksemplar}', [App\Http\Controllers\PerpusController::class, 'editbukuparam']);
Route::get('buku/hapus/{kode_eksemplar}', [App\Http\Controllers\PerpusController::class, 'hapusbukuparam']);



Route::get('anggota',[App\Http\Controllers\PerpusController::class, 'lihatanggota']);
Route::get('anggota/edit',[App\Http\Controllers\PerpusController::class, 'editanggota']);
Route::get('anggota/hapus',[App\Http\Controllers\PerpusController::class, 'hapusanggota']);
Route::get('anggota/edit/{nik}', [App\Http\Controllers\PerpusController::class, 'editanggotaparam']);
Route::get('anggota/hapus/{nik}', [App\Http\Controllers\PerpusController::class, 'hapusanggotaparam']);



Route::post('buku/tambah/process',[App\Http\Controllers\PerpusController::class, 'insertbuku']);

Route::post('buku/edit/process',[App\Http\Controllers\PerpusController::class, 'updatebuku']);
Route::post('buku/hapus/process',[App\Http\Controllers\PerpusController::class, 'deletebuku']);
Route::post('anggota/tambah/process',[App\Http\Controllers\PerpusController::class, 'insertanggota']);
Route::post('anggota/edit/process',[App\Http\Controllers\PerpusController::class, 'updateanggota']);
Route::post('anggota/hapus/process',[App\Http\Controllers\PerpusController::class, 'deleteanggota']);


Route::get('peminjaman',[App\Http\Controllers\PerpusController::class, 'peminjaman']);
Route::get('peminjaman/tambah',[App\Http\Controllers\PerpusController::class, 'tambahpeminjaman']);
Route::get('peminjaman/hapus/{id_transaksi}', [App\Http\Controllers\PerpusController::class, 'hapuspeminjamanparam']);
Route::get('peminjaman/hapus',[App\Http\Controllers\PerpusController::class, 'hapuspeminjaman']);
Route::post('peminjaman/tambah/process',[App\Http\Controllers\PerpusController::class, 'tambahprocess']);
Route::post('peminjaman/hapus/process',[App\Http\Controllers\PerpusController::class, 'hapusprocess']);

Route::get('pengembalian',[App\Http\Controllers\PerpusController::class, 'pengembalian']);
Route::get('pengembalian/kembali/{id_transaksi}',[App\Http\Controllers\PerpusController::class, 'pengembalianparam']);
Route::post('pengembalian/kembali/process',[App\Http\Controllers\PerpusController::class, 'pengembalianprocess']);




Route::get('pengguna',[App\Http\Controllers\PerpusController::class, 'mainpengguna']);
Route::get('pengguna/buku',[App\Http\Controllers\PerpusController::class, 'penggunalihatbuku']);
Route::post('pengguna/pinjambuku/process',[App\Http\Controllers\PerpusController::class, 'pinjambukuprocess']);
Route::get('pengguna/pengembalian',[App\Http\Controllers\PerpusController::class, 'pengembaliananggota']);
Route::get('pengguna/pengembalian/{id_transaksi}',[App\Http\Controllers\PerpusController::class, 'pengembaliananggotaparam']);
Route::post('pengguna/pengembalian/process',[App\Http\Controllers\PerpusController::class, 'pengembaliananggotaprocess']);
Route::get('pengguna/profile',[App\Http\Controllers\PerpusController::class, 'profile']);
Route::post('login',[App\Http\Controllers\PerpusController::class, 'login']);
Route::post('register/tambahakun',[App\Http\Controllers\PerpusController::class, 'tambahakun']);
Route::get('logout',[App\Http\Controllers\PerpusController::class, 'logout']);