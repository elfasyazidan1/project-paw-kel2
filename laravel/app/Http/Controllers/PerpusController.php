<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use App\Models\akun;
use Illuminate\Support\Facades\Session;


class PerpusController extends Controller
{
        public function lihatbuku()
    {   
        
        if(!empty(session(['id_akun']))){
            $perpus = DB::select('CALL lihatbuku()');

        return view('buku.lihatbuku', ['perpus' => $perpus]);
        }else return view('login');
        
    }
    public function editbuku()
    {
        if(!empty(session(['id_akun']))){
            return view('buku.editbuku');
        }else return view('login');
        
        
    }
    public function editbukuparam($kode_eksemplar)
    {   
        if(!empty(session(['id_akun']))){
        $perpus = DB::table('buku')
                    ->where('kode_eksemplar', '=', $kode_eksemplar)
                    ->select('kode_eksemplar')
                    ->first(); // Menggunakan first() karena kita mengharapkan satu hasil saja

        return view('buku.editbuku', ['perpus' => $perpus, 'kode_eksemplar' => $kode_eksemplar]);
        }else return view('login');
        
    }


    public function hapusbuku()
    {
        if(!empty(session(['id_akun']))){
            return view('buku.hapusbuku');
        }else return view('login');
        
        
    }

    public function hapusbukuparam($kode_eksemplar)
    {   
        if(!empty(session(['id_akun']))){
            $perpus = DB::table('buku')
                    ->where('kode_eksemplar', '=', $kode_eksemplar)
                    ->select('kode_eksemplar')
                    ->first();

            return view('buku.hapusbuku', ['perpus' => $perpus, 'kode_eksemplar' => $kode_eksemplar]);
        }else return view('login');
        
    }

    public function peminjaman()
    {   
        if(!empty(session(['id_akun']))){
            $peminjamandata = DB::select("CALL TampilTransaksi()");
            return view('peminjaman.lihatpeminjaman', ['peminjamandata' => $peminjamandata]);
        }else return view('login');

    }
    
    public function lihatanggota()
    {   
        if(!empty(session(['id_akun']))){
            $anggotaData = DB::table('anggota')
                ->select('nik', 'nama', 'email', 'alamat', 'telepon', 'jenis_kelamin')
                ->get();

            return view('anggota.lihatanggota', ['anggotaData' => $anggotaData]);
        }else return view('login');

    }
    
    public function editanggota()
    {
        if(!empty(session(['id_akun']))){
            return view('anggota.editanggota');
        }else return view('login');

    }
    public function editanggotaparam($nik)
    {   
        if(!empty(session(['id_akun']))){
            $anggotaData = DB::table('anggota')
                ->select('nik', 'nama', 'email', 'alamat', 'telepon', 'jenis_kelamin')
                ->where('nik', '=', $nik)
                ->first();

            return view('anggota.editanggota', ['anggotaData' => $anggotaData,'nik' => $nik]);
        }else return view('login');

    }
    public function hapusanggota()
    {
        if(!empty(session(['id_akun']))){
            return view('anggota.hapusanggota');
        }else return view('login');

    }
    public function hapusanggotaparam($nik)
    {   
        if(!empty(session(['id_akun']))){
            $anggotaData = DB::table('anggota')
                        ->select('nik', 'nama', 'email', 'alamat', 'telepon', 'jenis_kelamin')
                        ->where('nik', '=', $nik)
                        ->first();

            return view('anggota.hapusanggota', ['anggotaData' => $anggotaData,'nik' => $nik]);
        }else return view('login');
        
    }
    public function insertbuku(Request $request)
    {
        // Ambil data dari form
        $kode_eksemplar = $request->input('kode_eksemplar');
        $penerbit = $request->input('penerbit');
        $judul = $request->input('judul');
        $jumlah = $request->input('jumlah');
        $pengarang = $request->input('pengarang');

        // Panggil stored procedure InsertBuku dari database
        DB::select('CALL InsertBuku(?, ?, ?, ?, ?)', [$kode_eksemplar, $penerbit, $judul, $jumlah,$pengarang]);

        // Redirect ke lihatbuku dengan pesan sukses
        return redirect('buku')->with('status', 'Buku berhasil ditambahkan');
    }
    public function updatebuku(Request $request)
    {
        try {
            $kode_eksemplar = $request->input('kode_eksemplar');
            $penerbit = $request->input('penerbit');
            $judul = $request->input('judul');
            $jumlah = $request->input('jumlah');
            $pengarang = $request->input('pengarang');
            // Check if the book with the specified kode_eksemplar exists
            $cek = DB::table('buku')
                ->select('kode_eksemplar')
                ->where('kode_eksemplar', '=', $kode_eksemplar)
                ->first();
                
            if (is_null($cek)) {
                return redirect('buku')->with('status', 'Gagal mengupdate buku. Kode eksemplar tidak ditemukan.');
            }
            DB::select('CALL updatebuku(?, ?, ?, ?, ?)', [$kode_eksemplar, $penerbit, $judul, $jumlah,$pengarang]);
    
            return redirect('buku')->with('status', 'Buku berhasil diupdate');
        } catch (\Exception $e) {
            // Jika terjadi exception, tangkap dan berikan pesan kesalahan
            return redirect('buku')->with('status', 'Terjadi kesalahan: ' . $e->getMessage());
        }
    }

    public function deletebuku(Request $request){
        try {
            $kode_eksemplar = $request->input('kode_eksemplar');
            
            // Check if the book with the specified kode_eksemplar exists
            $cek = DB::table('buku')
                ->select('kode_eksemplar')
                ->where('kode_eksemplar', '=', $kode_eksemplar)
                ->first();
                
            if (is_null($cek)) {
                return redirect('buku')->with('status', 'Gagal menghapus buku. Kode eksemplar tidak ditemukan.');
            }
            DB::select('CALL deletebuku(?)', [$kode_eksemplar]);
    
            return redirect('buku')->with('status', 'Buku berhasil dihapus');
        } catch (\Exception $e) {
            // Jika terjadi exception, tangkap dan berikan pesan kesalahan
            return redirect('buku')->with('status', 'Terjadi kesalahan: ' . $e->getMessage());
        }
    }
    public function insertanggota(Request $request)
    {
        // Ambil data dari form
        $NIK = $request->input('NIK');
        $nama = $request->input('nama');
        $email = $request->input('email');
        $alamat = $request->input('alamat');
        $telepon = $request->input('telepon');
        $jenisKelamin = $request->input('jenisKelamin');

        // Panggil stored procedure InsertBuku dari database
        DB::select('CALL InsertAnggota(?, ?, ?, ?, ?, ?)', [$NIK, $nama, $email, $alamat, $telepon, $jenisKelamin]);
        return redirect('anggota')->with('status', 'Data berhasil ditambahkan');
    }
    public function updateanggota(Request $request)
    {
        try {
            $NIK = $request->input('NIK');
            $nama = $request->input('nama');
            $email = $request->input('email');
            $alamat = $request->input('alamat');
            $telepon = $request->input('telepon');
            $jenisKelamin = $request->input('jenisKelamin');
            // Check if the book with the specified kode_eksemplar exists
            $cek = DB::table('anggota')
            ->select('nik')
            ->where('nik', '=', $request)
            ->first();
                
            if (is_null($cek)) {
                return redirect('anggota')->with('status', 'Gagal mengupdate data. anggota tidak ditemukan.');
            }
            DB::select('CALL updateAnggota(?, ?, ?, ?, ?, ?)', [$NIK, $nama, $email, $alamat, $telepon, $jenisKelamin]);
    
            return redirect('anggota')->with('status', 'Data berhasil diupdate');
        } catch (\Exception $e) {
            // Jika terjadi exception, tangkap dan berikan pesan kesalahan
            return redirect('anggota')->with('status', 'Terjadi kesalahan: ' . $e->getMessage());
        }
    }
    public function deleteanggota(Request $request)
    {
        try {
            $NIK = $request->input('NIK');
            
            $cek = DB::table('anggota')
            ->select('nik')
            ->where('nik', '=', $NIK)
            ->first();
                
            if (is_null($cek)) {
                return redirect('anggota')->with('status', 'Gagal menghapus data. anggota tidak ditemukan.');
            }
            DB::select('CALL deleteanggota(?)', [$NIK]);
            return redirect('anggota')->with('status', 'Data berhasil dihapus');
        } catch (\Exception $e) {
            // Jika terjadi exception, tangkap dan berikan pesan kesalahan
            return redirect('anggota')->with('status', 'Terjadi kesalahan: ' . $e->getMessage());
        }
    }



    public function tambahpeminjaman()
    {
        return view('peminjaman.tambahpeminjaman');
    }
    public function editpeminjaman()
    {
        return view('peminjaman.editpeminjaman');
    }
    public function hapuspeminjaman()
    {
        return view('peminjaman.hapuspeminjaman');
    }

    public function tambahprocess(Request $request)
    {
        // Validate the form data
        $request->validate([
            'NIK' => 'required|numeric',
            'angka' => 'required|numeric',
            'waktu_pinjam' => 'required|date',
            'kode_eksemplar.*' => 'required', // You may need to adjust this validation rule
        ]);

        // Retrieve form data
        $NIK = $request->input('NIK');
        $angka = $request->input('angka');
        $waktu_pinjam = $request->input('waktu_pinjam');
        $kode_eksemplar = $request->input('kode_eksemplar');

        $book_copy_codes = implode(',', $kode_eksemplar);

        $result = DB::select('CALL addtransaksi(?, ?, ?)', [$NIK, $book_copy_codes, $waktu_pinjam]);

        return redirect('peminjaman')->with('status', 'data berhasil ditambahkan');
        
    }
    public function hapusprocess(Request $request)
    {   
        try {
            $id_transaksi = $request->input('id_transaksi');
            $cek = DB::table('transaksi')
            ->select('id_transaksi')
            ->where('id_transaksi', '=', $request)
            ->first();
                
            if (is_null($cek)) {
                return redirect('peminjaman')->with('status', 'Gagal menghapus data. id tidak ditemukan.');
            }
            DB::select('CALL deleteanggota(?)', [$NIK]);
            return redirect('peminjaman')->with('status', 'Data berhasil dihapus');
        } catch (\Exception $e) {
            // Jika terjadi exception, tangkap dan berikan pesan kesalahan
            return redirect('peminjaman')->with('status', 'Terjadi kesalahan: ' . $e->getMessage());
        }
    }
    public function hapuspeminjamanparam($id_transaksi)
    {
        $anggotaData = DB::table('transaksi')
            ->select('id_transaksi')
            ->where('id_transaksi', '=', $id_transaksi)
            ->first();

        return view('peminjaman.hapuspeminjaman', ['id_transaksi' => $id_transaksi]);
    }
    public function pengembalian()
    {
        $peminjamandata = DB::select("CALL TampilTransaksi()");
        return view('pengembalian.lihatpengembalian', ['peminjamandata' => $peminjamandata]);
    }
    public function pengembalianparam($id_transaksi)
    {
        $anggotaData = DB::table('transaksi')
            ->select('id_transaksi')
            ->where('id_transaksi', '=', $id_transaksi)
            ->first();
        return view('pengembalian.kembali', ['id_transaksi' => $id_transaksi]);
    }
    public function pengembalianprocess(Request $request)
    {
        $request->validate([
            'id_transaksi' => 'required|numeric',
            'waktu_kembali' => 'required|date'
        ]);
        // Retrieve form data
        $id_transaksi = $request->input('id_transaksi');
        $waktu_kembali = $request->input('waktu_kembali');
        $result = DB::select("CALL kembalikan_buku('$id_transaksi','$waktu_kembali')");
        return redirect('peminjaman')->with('status', 'data berhasil dihapus');
    }
    public function home(){
        $jumlah_anggota = DB::select("call jumlahanggota()");
        $jumlah_buku = DB::select("CALL JumlahBuku()");
        $jumlah_peminjaman = DB::select("call jumlahpeminjaman()");
        return view('home',['jumlah_buku' => $jumlah_buku,'jumlah_peminjaman' => $jumlah_peminjaman,'jumlah_anggota' => $jumlah_anggota]);
    }
    public function mainpengguna(){
        $jumlah_anggota = DB::select("call jumlahanggota()");
        $jumlah_buku = DB::select("CALL JumlahBuku()");
        $jumlah_peminjaman = DB::select("call jumlahpeminjaman()");
        return view('pengguna.homepengguna',['jumlah_buku' => $jumlah_buku,'jumlah_peminjaman' => $jumlah_peminjaman,'jumlah_anggota' => $jumlah_anggota]);
    }

    public function login(Request $request){
        $email = $request->input('email');
        $password = $request->input('password');
    
        // Attempt to authenticate the user using stored procedure
        $akun = DB::select("call pilihakun('$email','$password')");
    
        if (!empty($akun)) {
            $akun = $akun[0]; // Assuming the first row of the result set is what you need
    
            // Authentication successful
            if ($akun->role_ == 'admin') {
                session(['id_akun' => $akun->id_akun]);
                return redirect('home');
            } elseif ($akun->role_ == 'anggota') {
                session(['id_akun' => $akun->id_akun]);
                return redirect("pengguna")->with('status', 'berhasil login');
            }
        } else {
            // Authentication failed, redirect back to login page
            return redirect()->back()->with('error', 'Invalid email or password');
        }
    }
    public function tambahakun(Request $request)
    {
        // Ambil data dari form
        $NIK = $request->input('NIK');
        $nama = $request->input('nama');
        $email = $request->input('email');
        $password = $request->input('password');
        $alamat = $request->input('alamat');
        $telepon = $request->input('telepon');
        $jenisKelamin = $request->input('jenisKelamin');

        // Panggil stored procedure InsertBuku dari database
        DB::select('CALL tambahakun(?, ?, ?, ?, ?, ?, ?)', [$NIK, $nama, $email, $alamat, $telepon, $jenisKelamin,$password]);
        return redirect('/');
    }
    public function pinjambukuprocess(Request $request)
    {   
        $akun = session('id_akun');
        $angka = $request->input('angka');
        $waktu_pinjam = $request->input('waktu_pinjam');
        $kode_eksemplar = $request->input('kode_eksemplar');

        $book_copy_codes = implode(',', $kode_eksemplar);
        $id_anggota = DB::select("call findidanggotabyidakun($akun)");
        $id_anggota_value = $id_anggota[0]->ID_Anggota;
        
        $result = DB::select('CALL pinjambuku(?, ?, ?)', [$id_anggota_value, $book_copy_codes, $waktu_pinjam]);
        
        return redirect('pengguna')->with('status', 'data berhasil ditambahkan');
        
    }
    public function penggunalihatbuku()
    {
        // Panggil stored procedure lihatbuku dari database
        $perpus = DB::select('CALL lihatbuku()');

        return view('pengguna.buku', ['perpus' => $perpus]);
    }
    public function pengembaliananggota()
    {   
        $akun = session('id_akun');
        $id_anggota = DB::select("call findidanggotabyidakun($akun)");
        $id_anggota_value = $id_anggota[0]->ID_Anggota;
        $peminjamandata = DB::select("CALL TampilTransaksianggota($id_anggota_value)");
        return view('pengguna.pengembalianbuku', ['peminjamandata' => $peminjamandata]);
    }
    public function pengembaliananggotaparam($id_transaksi)
    {
        $anggotaData = DB::table('transaksi')
            ->select('id_transaksi')
            ->where('id_transaksi', '=', $id_transaksi)
            ->first();
        return view('pengguna/formpengembalian', ['id_transaksi' => $id_transaksi]);
    }
    public function pengembaliananggotaprocess(Request $request)
    {
        $request->validate([
            'id_transaksi' => 'required|numeric',
            'waktu_kembali' => 'required|date'
        ]);
        // Retrieve form data
        $id_transaksi = $request->input('id_transaksi');
        $waktu_kembali = $request->input('waktu_kembali');
        $result = DB::select("CALL kembalikan_buku('$id_transaksi','$waktu_kembali')");
        return redirect('pengguna/pengembalian')->with('status', 'Pengembalian Berhasil');
    }
    public function profile()
    {   
        $akun = session('id_akun');
        $id_anggota = DB::select("call findidanggotabyidakun($akun)");
        $id_anggota_value = $id_anggota[0]->ID_Anggota;
        // Panggil stored procedure lihatbuku dari database
        $profile = DB::select("CALL tampilprofile($id_anggota_value)");

        return view('pengguna.profilepengguna', ['profile' => $profile]);
    }
    public function logout()
    {   
        Session::forget(['akun', 'id_akun']);

        return redirect('/');
    }
    

}
