@extends('mainpengguna')

@section('title','dashboard')

@section('breadcrumbs')


@endsection

@section('content')
<div class="container-fluid">
    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Profile</strong>
                        </div>
                        <div class="card-body">
                            <div id="pay-invoice">
                                <div class="card-body">
                                    <div class="card-title">
                                        <h3 class="text-center">Anggota</h3>
                                    </div>
                                    <form method="post" action="">
                                        @csrf <!-- Token CSRF untuk keamanan -->
                                        
                                        <div class="form-group">
                                            <label for="inputNIK" class="control-label mb-1">NIK</label>
                                            <input id="inputNIK" name="NIK" type="text" class="form-control" value="{{ $profile[0]->nik }}" readonly>
                                        </div>
                                    
                                        <div class="form-group has-success">
                                            <label for="inputNama" class="control-label mb-1">Nama</label>
                                            <input id="inputNama" name="nama" type="text" class="form-control" value="{{ $profile[0]->nama }}" readonly>
                                        </div>
                                    
                                        <div class="form-group">
                                            <label for="inputEmail" class="control-label mb-1">Email</label>
                                            <input id="inputEmail" name="email" type="email" class="form-control" value="{{ $profile[0]->email }}" readonly>
                                        </div>
                                    
                                        <div class="form-group">
                                            <label for="inputAlamat" class="control-label mb-1">Alamat</label>
                                            <input id="inputAlamat" name="alamat" type="text" class="form-control" value="{{ $profile[0]->alamat }}" readonly>
                                        </div>
                                    
                                        <div class="form-group">
                                            <label for="inputTelepon" class="control-label mb-1">No. Telpon</label>
                                            <input id="inputTelepon" name="telepon" type="tel" class="form-control" value="{{ $profile[0]->telepon }}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputjeniskelamin" class="control-label mb-1">No. Telpon</label>
                                            <input id="inputjeniskelamin" name="jeniskelamin" type="tel" class="form-control" value="{{ $profile[0]->jenis_kelamin }}" readonly>
                                        </div>
                                    
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection